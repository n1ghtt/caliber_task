<?php

require_once realpath(__DIR__ . '/vendor/autoload.php');
use \SEOstats\Services\Social as Social;
use \SEOstats\Services\Alexa as Alexa;
use \SEOstats\Services\Google as Google;


class SocialResponse {

function __construct() { 
	 
}

public function getSocials($url) {
try {

    // Create a new SEOstats instance.
    $seostats = new \SEOstats\SEOstats;


    // Bind the URL to the current SEOstats instance.
    if ($seostats->setUrl($url)) {

    	$backlinks = Google::getBacklinksTotal();

        $google = Social::getGooglePlusShares();
        

        $twiter = Social::getTwitterShares();
        
        
        $facebook = Social::getFacebookShares();
        

        $pininterest = Social::getPinterestShares();
        
        
        $linkedin = Social::getLinkedInShares();

        
     	//$backlinks = Google::getBacklinksTotal();

        $response = array("url" => $url ,"Backlinks" => $backlinks ,"google_shares" => $google, "twitter_shares" => $twiter, "linkedin_shares" => $linkedin);
       
       	foreach($facebook as $k => $v )
        {
        	$response['fb_'.$k] = $v;
        }

        return $response;
    }
}

catch (\Exception $e) {
    echo 'Caught SEOstatsException: ' .  $e->getMessage();
}

}
}


