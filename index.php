<!DOCTYPE html>
<html>
<body>
	<head>
		<meta charset="utf-8">
   		<title>Caliber SEO Metrics Demo</title>

    	<meta name="description" content="">
    	<!--<meta name="viewport" content="width=device-width">-->

    	<!-- local CSS files -->
    	
    	<link rel="stylesheet" type="text/css" href="css/style.css"/>
    	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.1/css/materialize.min.css">
    	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.4.0/animate.min.css">
		
		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" integrity="sha256-MfvZlkHCEqatNoGiOXveE8FIwMzZg4W85qfrfIFBfYc= sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==" crossorigin="anonymous">
		
		<!-- external CSS files-->
		
	</head>
	
	<div id="container" class="container center-align" style="padding-top: 50px;">
		<h1 class="wow zoomInRight" style='color: #F2F2F2;'>Bulk Domain Metric Tool</h1>
		<h5 id='information'style='color: #F2F2F2;'>Please paste links in the text area and hit Submit!</h5>
    	<textarea style="opacity:0.8;" class="form-control wow bounceInLeft" data-wow-duration="0.5s" data-wow-delay="0.5s" placeholder="One Link per line " rows="9" id="linkInput"></textarea> 
    	<br>
          <button class="btn waves-effect waves-light wow bounceInLeft" data-wow-duration="0.5s" data-wow-delay="0.7s" type="submit" name="action" id="parseLink">Submit
    	<i class="material-icons right"></i>
  	</button>
        </div>
    </div>

  </div>  
</div>
	 
	 <div class="row" id="footer">   
	 	<div class="col-md-4">
		<footer style="color: #F2F2F2; margin-left: 80px;">
			<i class="fa fa-copyright"></i> Andrejs Gubars - 2015
		</footer>
	</div>
	</div>
	<!-- Javascript libraries-->
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0-alpha1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.1/js/materialize.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
	<script type="text/javascript" src="js/foo.js"></script>

	<script src="js/tableExport.js"></script>

	<script src="js/wow.min.js"></script>
              <script>
              new WOW().init();
    </script>
</body>
</html>