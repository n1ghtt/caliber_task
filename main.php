<?php

include 'parser.php';

class ParseJson extends SocialResponse
{
    public $postInput;

    public function __construct()
    {
        //esli v SocialREsponse est construct to sleduwuju stroicku uncomment
        parent::__construct();
    }

    public function setPost($post)
    {
        $this->postInput = $post;
    }

    public function parseArray()
    {
        $data = $this->postInput;
        //$data = json_decode('["http://facebook.com/","http://google.com/"]');
        $newArray = array();
        foreach ($data as $v) {
            array_push($newArray, $v);
        }

        return $newArray;
    }

    public function finalArray()
    {
        $parsedArray = $this->parseArray();
        $final = array();
        foreach ($parsedArray as $v) {
            array_push($final, $this->getSocials($v));
        }

        return $final;
    }
}

$data = json_decode($_POST['data']);

if(isset($data)) {

    $gavno = new ParseJson();

    $gavno->setPost($data);

    $response = $gavno->finalArray();

   echo json_encode($response);

}
