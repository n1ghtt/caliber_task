$(document).ready( function(){
	$("#parseLink").on("click", function(){
		var textBox = $("#linkInput").val();
		var newTextBox = textBox.split("\n");
		$('#parseLink').replaceWith( "<div class='preloader-wrapper big active'><div class='spinner-layer spinner-blue'><div class='circle-clipper left'><div class='circle'></div></div><div class='gap-patch'><div class='circle'></div></div><div class='circle-clipper right'><div class='circle'></div></div></div><div class='spinner-layer spinner-red'><div class='circle-clipper left'><div class='circle'></div></div><div class='gap-patch'><div class='circle'></div></div><div class='circle-clipper right'><div class='circle'></div></div></div><div class='spinner-layer spinner-yellow'><div class='circle-clipper left'><div class='circle'></div></div><div class='gap-patch'><div class='circle'></div></div><div class='circle-clipper right'><div class='circle'></div></div></div><div class='spinner-layer spinner-green'><div class='circle-clipper left'><div class='circle'></div></div><div class='gap-patch'><div class='circle'></div></div><div class='circle-clipper right'><div class='circle'></div></div></div></div>" );
		$('.form-control').replaceWith( "<div class='clear'></div>");
		$('div#footer').replaceWith( "<div class='clear'></div>");
		$('#information').replaceWith( "<h5 class='wow rollIn' style='color: #F2F2F2;'>Loading ... have a cup of tea <i class='fa fa-smile-o'></i> </h5>" );
		$.ajax( {
	        type: "POST",
	        url: "../main.php",
	        data: {data: JSON.stringify(newTextBox)},
			success: function(response){
	        	
	        	parsedArray = response;
	        	handleArray(parsedArray);
	        }
    });
		
	});

	function handleArray(returnedArray)
	{
		 
		var arr = returnedArray;

		//parse JSON to readable array for jQuery
		var normalArray = jQuery.parseJSON(arr);
		//magic happens here :)
		//getting data from tables and building tables!

		$("div#container").html( "<h3 style='color: #F2F2F2;'>RESULT</h3><div style='opacity:0.8;' class='table-responsive-vertical highlight shadow-z-1'>" );
		$( "<div class='row' style='margin-bottom: -20px;'><div style='float: right; margin-right: 7px;'><button class='offset-s2 btn waves-effect waves-light wow bounceInLeft' data-wow-duration='0.3s' data-wow-delay='0.2s' id='export'>Export as .xls<i class='material-icons right'></i></button></div></div>" ).insertBefore(" h3 ");
		$("div.table-responsive-vertical").append( "<table id='table' class=' highlight table table-hover table-mc-light-blue'>" );
		$("table#table").append( "<thead class='wow bounceIn' data-wow-duration='0.3s' data-wow-delay='0.2s' id='table_head'>" );
		$("thead#table_head").append( "<tr id='tbl_row'>" );
		$("tr#tbl_row").append( "<th><i class='fa fa-list'><span style='visibility: hidden;'>Number</span></i></th><th>Link</th><th><i class='fa fa-arrow-left'></i><i class='fa fa-link'><span style='visibility: hidden;'>Backlinks</span></i></th><th><i class='fa fa-google-plus'><span style='visibility: hidden;'>GooglePlusOnes</span></i></th><th><i class='fa fa-twitter'><span style='visibility: hidden;'>ReTweets</span></i></th><th><i class='fa fa-linkedin'><span style='visibility: hidden;'>LinkedIn</span></i></th><th><i class='fa fa-facebook'><span style='visibility: hidden;'>Facebook Shares</span></i></th><th><i class='fa fa-thumbs-o-up'><span style='visibility: hidden;'>Facebook_Likes</span></i></th></tr></thead>" );
		$("table#table").append(" <tbody id='tbl'>" );
				for(var i = 0; i < normalArray.length; i++)
				{
					var delay=i/5;
					var num=i+1;
					$("tbody#tbl").append(" <tr class='wow bounceInLeft' data-wow-duration='0.3s' data-wow-delay=" + delay + "s id='t_row'><td> " + num + "</td><td data-title='Link'><a href=" + normalArray[i].url + " target='_blank' >" + normalArray[i].url + "</td><td data-title='Backlinks'>" + normalArray[i].Backlinks + "</td><td data-title='GooglePlusOnes'>" + normalArray[i].google_shares + "</td><td data-title='ReTweets'>" + normalArray[i].twitter_shares + "</td><td data-title='LinkedIn'>" + normalArray[i].linkedin_shares + "</td><td data-title='FB_Shares'>" + normalArray[i].fb_share_count + "</td><td data-title='FB_Likes'>" + normalArray[i].fb_like_count + "</td>" );

				}
				$("tr#t_row").append(" </tr></tbody></table></div> ");
				$('button#export').bind('click', function (e) {             
            		$('table#table').tableExport({ type: 'excel', escape: 'false' });  
       			 }); 
				$( "<footer style='color: #F2F2F2; margin-left: 80px;''><i class='fa fa-copyright'></i> Andrejs Gubars - 2015</footer>" ).insertAfter('table#table');
	}
});